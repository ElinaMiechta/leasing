describe("Test Links", () => {
  it("open landing page", () => {
    cy.visit("http://localhost:3000/");
  });
  it("open cars page", () => {
    cy.visit("http://localhost:3000/cars");
  });
  it("should_test_Cars_href", () => {
    const url = "http://localhost:3000";
    cy.visit("http://localhost:3000/")
      .get(".iENldC>a")
      .eq(1)
      .should("have.text", "Cars")
      .invoke("attr", "href")
      .then((href) => {
        cy.visit(url + href);
      })
      .get(".card")
      .should("be.visible");
  });
});

describe("Dark Mode Tests", () => {
  it("should_change_bgColor", () => {
    cy.visit("http://localhost:3000/")
      .get(".jtLNNG")
      .click()
      .get("body")
      .should("have.css", "background-color", "rgb(11, 11, 21)")
      .get(".gTninB")
      .click()
      .get("body")
      .should("have.css", "background-color", "rgb(255, 255, 255)");
  });

  it("should_change_btns_bgColor", () => {
    cy.visit("http://localhost:3000/")
      .get(".jtLNNG")
      .click()
      .get(".ecONUx")
      .should("have.css", "background-color", "rgb(205, 133, 63)")
      .should("have.css", "color", "rgb(0, 13, 26)")
      .get(".gTninB")
      .click()
      .get(".gpVPXc")
      .should("have.css", "background-color", "rgb(11, 11, 21)")
      .should("have.css", "color", "rgb(255, 255, 255)");
  });
});

describe("Contact Modal Visibility Tests", () => {
  it("should_toggle_modal_on_homePage", () => {
    cy.visit("http://localhost:3000/")
      .get(".dcdRpb>a")
      .click()
      .get(".modal")
      .should("be.visible")
      .get(".modal-exit")
      .click()
      .get("#root")
      .children()
      .should("not.have.class", "modal");
  });
  it("should_toggle_modal_on_carsPage", () => {
    cy.visit("http://localhost:3000/cars")
      .get(".dcdRpb>a")
      .click()
      .get(".modal")
      .should("be.visible")
      .get(".modal-exit")
      .click()
      .get("#root")
      .children()
      .should("not.have.class", "modal");
  });
});
describe("Product Page Data Test", () => {
  const url = "http://localhost:3000/";
  it("should_open_productPage_with_data", () => {
    cy.visit("http://localhost:3000/cars")
      .get(".hwmAnx>.card")
      .eq(0)
      .get(".card-body>a")
      .invoke("attr", "href")
      .then((href) => {
        cy.visit(url + href);
      })
      .get(".dyiDec")
      .should("not.be.empty");
  });
});
