import { useState } from "react";
import { productData } from "../components/data/productData";

export const useCars = () => {
  const [cars, setCars] = useState(productData);

  const filterArrayByName = (prop) => {
    if (prop === "All") {
      return cars;
    }

    const filtered = cars.filter((el) => {
      return el.mark === prop;
    });

    return filtered;
  };

  const filterById = (id) => {
    let target = cars.filter((car) => {
      return car.id == id;
    });

    return target;
  };

  return { cars, filterArrayByName, filterById };
};

export default useCars;
