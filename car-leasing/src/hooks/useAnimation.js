import { useState } from "react";

export const useAnimation = (coordinate) => {
  const [activate, setActivate] = useState(false);

  const animationManage = () =>
    window.scrollY >= coordinate ? setActivate(true) : setActivate(false);

  return { activate, animationManage };
};

