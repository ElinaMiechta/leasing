import { useState } from "react";
import axios from 'axios';

const useForm = validate => {
  const [data, setData] = useState({
    name: "",
    email: "",
    phone: "",
    msg: ""
  });
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [errors, setErrors] = useState({ default: "default" });

  const handleChange = e => {
    const { name, value } = e.target;
    setData({
      ...data,
      [name]: value
    });
  };

  const handleSubmit = e => {
    e.preventDefault();
    setErrors(validate(data));
    if (Object.keys(errors).length === 0) {
      setIsSubmitted(true); 
      axios.post('http://localhost:3000/clients', data)
      .then(res => console.log(res))
    }
  };

  return { handleChange, data, handleSubmit, errors, isSubmitted };
};

export default useForm;
