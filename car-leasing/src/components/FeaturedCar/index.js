import { useState, useRef, useEffect } from "react";
import {
  Container,
  TitleWrapper,
  MainTitle,
  DescrTitle,
  BtnWrapper,
  BtnOrder,
  BtnDetails,
  DotWrapper,
  Dot,
  FeaturedImage,
  FeaturedSlider
} from "./FeaturedCarElements";
import "./index.css";

const FeaturedCar = ({ slides }) => {
  const [current, setCurrent] = useState(0);
  const length = slides.length;
  const timeout = useRef(null);
  const dotsRef = useRef(null);

  useEffect(() => {
    const switchSlides = () => {
      setCurrent(current === length - 1 ? 0 : current + 1);
    };
    timeout.current = setTimeout(switchSlides, 5000);
    return () => {
      clearTimeout(timeout.current);
    };
  });

  useEffect(() => {
    dotsRef.current.childNodes.forEach((child, index) => {
      index === current
        ? child.classList.add("active-dot")
        : child.classList.remove("active-dot");
    });
  }, [current]);

  const switchSlide = number => {
    setCurrent(number);
    dotsRef.current.childNodes.forEach((child, index) => {
      return index === number
        ? child.classList.add("active-dot")
        : child.classList.remove("active-dot");
    });
  };

  return (
    <Container>
      {slides.map((slide, index) => (
        <FeaturedSlider key={index}>
          {index === current && (
            <FeaturedImage src={slide.image}></FeaturedImage>
          )}
        </FeaturedSlider>
      ))}

      <TitleWrapper>
        <MainTitle>Model X</MainTitle>
        <DescrTitle>Modern Super Nova</DescrTitle>
      </TitleWrapper>
      <BtnWrapper>
        <BtnOrder to="#">Order</BtnOrder>
        <BtnDetails to="#">Details</BtnDetails>
      </BtnWrapper>
      <DotWrapper ref={dotsRef}>
        <Dot onClick={() => switchSlide(0)} className="active-dot" />
        <Dot onClick={() => switchSlide(1)} />
        <Dot onClick={() => switchSlide(2)} />
      </DotWrapper>
    </Container>
  );
};

export default FeaturedCar;
