import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {BsDot} from 'react-icons/bs';

export const Container = styled.div`
width: 100%;
height:100vh;
display:flex;
justify-content: flex-start;
flex-direction:column;
background-size: cover;
position: relative;
overflow: hidden;
`;

export const FeaturedSlider = styled.div`
width:100%;
height:100%;
z-index:-1;
position: absolute;
`;

export const FeaturedImage = styled.img`
width:100%;
height:100%;
z-index:-1;
object-fit: cover;
`;

export const TitleWrapper = styled.div`
width:100%;
display:flex;
justify-content: center;
flex-direction: column;
align-items: center;
margin-top:5rem;
position: absolute;
`;
export const MainTitle = styled.span`
font-size: clamp(1.5em,6vw,3em);
color: #0b0b15;
cursor: pointer;
`;
export const DescrTitle = styled.span`
font-size: clamp(1em,4vw,2em);
color: #0b0b15;
cursor: pointer;
`;
export const BtnWrapper = styled.div`
width:100%;
display:flex;
justify-content:center;
align-items: center;
position: absolute;
top: 80%;

@media only screen and (max-width: 460px) {
   justify-content: flex-start;  
   margin-left: -1rem; 
}
@media only screen and (max-width: 428px) {
    margin-left: -2rem;
}

`;
export const BtnOrder = styled(Link)`
font-size: clamp(1em,4vw,1.5em);
color: #fff;
cursor: pointer;
padding: 0px 56px;
background-color: #686b6dd6;
border-radius:50px;
margin: 0px 50px;
box-shadow: 2px 2px 6px #232323;

&:visited {
      color: color: #fff;
}

&:hover {
      color: #fff;
      box-shadow: 4px 4px 9px #232323;
}

@media only screen and (max-width: 428px) {
      padding: 0px 45px;
  }

  @media only screen and (max-width: 360px) {
      padding: 0px 36px;
      }

      @media only screen and (max-width: 345px) {
      padding: 0px 29px;
      }
`;
export const BtnDetails = styled(Link)`
font-size: clamp(1em,4vw,1.5em);
color: #0b0b15;
cursor: pointer;
padding: 0px 56px;
border-radius:50px;
background-color: #b8bfc3d6;
box-shadow: 2px 2px 6px #232323;

&:visited {
      color: color: #0b0b15;
}

&:hover {
      box-shadow: 4px 4px 9px #232323;
}

@media only screen and (max-width: 428px) {
      padding: 0px 45px;
  }

  @media only screen and (max-width: 360px) {
  padding: 0px 36px;
  }

  @media only screen and (max-width: 345px) {
      padding: 0px 29px;
      }

`;
export const DotWrapper = styled.div`
width:100%;
display:flex;
flex-direction:row;
align-items: center;
justify-content: center;
position: absolute;
bottom:0;
`;
export const Dot = styled(BsDot)`
color: #b8bfc3d6;
padding: 6px 12px;
font-size: 7em;

@media only screen and (max-width: 640px) {
      font-size:4em;
}

&:hover {
      color: #686b6dd6;
}
`;