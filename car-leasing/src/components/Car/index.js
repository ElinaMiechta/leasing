import React, { useState, useEffect } from "react";
import "./index.css";
import { Link } from "react-router-dom";
import { Loader } from "../Loader";
import { useCars } from "../../hooks/useCars";

const Car = ({ targetName }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const { filterArrayByName, cars } = useCars();

  useEffect(() => {
    const delay = setTimeout(() => setIsLoaded(true), 2000);

    return () => {
      clearTimeout(delay);
    };
  }, []);

  const handleClick = (carId) => {
    localStorage.setItem("id", carId);
  };

  return (
    <>
      {isLoaded ? (
        targetName ? (
          filterArrayByName(targetName).map((car, index) => (
            <div className="card" key={index}>
              <img src={car.img} className="card-img-top" alt={car.altimg} />
              <div className="card-body">
                <h5 className="card-title">{car.mark}</h5>
                <p className="card-model">{car.model}</p>
                <p className="card-price">{car.price}</p>
                <Link
                  to="/product"
                  className="btn btn-primary"
                  onClick={() => handleClick(car.id)}>
                  Details
                </Link>
              </div>
            </div>
          ))
        ) : (
          cars.map((car, index) => (
            <div className="card" key={index}>
              <img src={car.img} className="card-img-top" alt={car.altimg} />
              <div className="card-body">
                <h5 className="card-title">{car.mark}</h5>
                <p className="card-model">{car.model}</p>
                <p className="card-price">{car.price}</p>
                <Link
                  to="/product"
                  className="btn btn-primary"
                  onClick={() => handleClick(car.id)}>
                  Details
                </Link>
              </div>
            </div>
          ))
        )
      ) : (
        <Loader />
      )}
    </>
  );
};

export default Car;
