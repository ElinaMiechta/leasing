import styled , {css,keyframes} from 'styled-components';
import {AiOutlineLine} from 'react-icons/ai';

const flex = css`
display:flex;
flex-direction:column;
justify-content: center;
align-items: center;
`;

const fullSize = css`
width:100%;
height:100%;
`;


const slideTxtAnimation  = keyframes`
      from {
        transform: translateX(-100%)
      }
    
      to {
        transform: translateX(0%);
      }
    }
    `;

const textAnimation = keyframes`
from {
      transform: translateX(100%)
}

to {
      transform:translateX(0%)
}
`;


export const MainSection = styled.section`
${fullSize};
background-color: ${props => props.dark ? '#000' : "#fff"}
`;

export const  PresentationalContainer = styled.div`
width: 100%;
height: 80vh;
display:flex;
align-items:center;
position: relative;
background-image: url('${props => props.imgSrc}');
background-repeat: no-repeat;
background-size: cover;
filter: brightness(0.7);
position: relative;
text-align:center;

@media only screen and (max-width: 828px) {
      max-width:100%;
}

&:before {
      content: '';
      width:100%;
      height:10%;
      position:absolute;
      top:0;
      left:0;
      background-color: #000;
      filter: drop-shadow(20px 12px 6px black);
      opacity: 0.7;
}
`;


export const PresentationalHeader = styled.h2`
color: #fff;
font-weight: 600;
font-size: clamp(1.5em, 4vw,2em);
padding: 8px 14px;
position: absolute;
top:0;
left: 40%;
z-index:1;

@media only screen and (max-width: 398px) {
      left: 20%;    
}

`;
export const SliderInfo = styled.div`
margin:30px;
width:70%;
padding: 20px 30px;

@media only screen and (max-width: 708px) {
      width:100%;
      padding: 12px;
}
`;
export const Info = styled.h3`
color: ${props => props.dark ? '#fff' : "#242424"};
font-size:20px;
font-family: 'Montserrat',sans-serif;
animation: ${({ activate }) => (activate ? slideTxtAnimation : "none")} 2s linear 1;

@media only screen and (max-width: 708px) {
    font-size:16px;
}

`;

