import { useRef } from "react";
import CustomSlider from "../CustomSlider";
import Banner from "../ProductBanner";
import {
  PresentationalContainer,
  PresentationalHeader,
  MainSection,
  SliderInfo,
  Info,
} from "./ProductCardElements";
import "./index.css";
import Parallax from "../ParallaxContainer";
import { useCars } from "../../hooks/useCars";
import { useDarkMode } from "../../context/darkModeContext";
import InfoBanner from "../ProductInfoBanner.js";
import { useAnimation } from "../../hooks/useAnimation";

const ProductCard = () => {
  const { filterById } = useCars();
  const storageId = localStorage.getItem("id");
  const { darkMode } = useDarkMode();
  const slideInfoRef = useRef(null);
  const product = filterById(storageId)[0];
  const { activate, animationManage } = useAnimation(1220);

  window.addEventListener("scroll", () => {
    animationManage();
  });

  const productImages = [
    {
      img: product["img-ed"],
    },
    {
      img: product["img-s1"],
    },
    {
      img: product["img-s2"],
    },
  ];

  return (
    <MainSection dark={darkMode}>
      <Parallax
        top="0"
        height="100vh"
        bgSrc={product.img}
        header={product.mark}
        btnText=" Shop now"
      />
      <PresentationalContainer imgSrc="https://i.pinimg.com/originals/41/32/a1/4132a1ff1be7213649339de08b788b97.jpg">
        <PresentationalHeader>All new interiors</PresentationalHeader>
      </PresentationalContainer>
      <div className="slider">
        <CustomSlider data={productImages} />
        <SliderInfo>
          <Info dark={darkMode} ref={slideInfoRef} activate={activate}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat.
          </Info>
        </SliderInfo>
      </div>
      <Banner dir="row" img="bg/block1.jpeg" />
      <Banner dir="row-reverse" img="bg/block2.jpeg" />
      <Banner
        dir="row"
        img="https://image.winudf.com/v2/image1/Y29tLmx1eHVyeWNhcl9zY3JlZW5fMl8xNTQ5Njc3NTI1XzA5Nw/screen-2.jpg?fakeurl=1&type=.jpg"
      />
      <Parallax
        top="5rem"
        height="60vh"
        bgSrc="https://www.dimmitt.com/blogs/3093/wp-content/uploads/2018/10/2019-RollsRoyce-Wraith-3.jpeg"
        header=""
        btnText="Shop now"
      />
      <InfoBanner />
    </MainSection>
  );
};

export default ProductCard;
