import styled, {css} from 'styled-components/macro'
import {Link} from 'react-router-dom'
import {VscThreeBars} from 'react-icons/vsc'
import {CgSun} from 'react-icons/cg'
import {BiMoon} from 'react-icons/bi'

export const Nav = styled.nav`
height: 60px;
background-color: ${({active}) => (active ? '#CD853F' : '#00000040')};
display:flex;
justify-content: space-between;
padding: 1rem 2rem;
z-index: 9999;
position: fixed;
width:100%;
`;

const NavLink = css`
color: #fff;
display:flex;
align-items:center;
padding: 0 1rem;
height:100%;
cursor: pointer;
`;

export const Logo = styled(Link)`
${NavLink}
font-style: italic;
`;

export const MenuBars = styled(VscThreeBars)`
display:none;
font-size: 2em;
color: #fff;
cursor: pointer;
position: absolute;
top:0.5rem;
right:0;

@media only screen and (max-width:768px) {
      display:block;
}
`;

export const NavMenu = styled.div`
display:flex;
align-items:center;
margin-right: -48px;
user-select:none;

@media only screen and (max-width: 768px) {
      display:none;
}
`;

export const NavMenuLinks = styled(Link)`
${NavLink}
`;


export const NavBtn = styled.div`
display:flex;
align-items: center;
margin-right: 24px;

@media only screen and (max-width: 768px) {
      display:none;
}
`;

const themeIcon = css`
font-size: 2em;
color: #fff;

&:hover {
      transform: scale(1.1);
}

@media only screen and (max-width: 768px) {
     margin-right: 5rem;
}
`;

export const DarkThemeIcon = styled(BiMoon)`
${themeIcon}
`;
export const LightThemeIcon = styled(CgSun)`
${themeIcon}
`;



