import React, { useState, useEffect } from "react";
import {
  Nav,
  Logo,
  MenuBars,
  NavMenu,
  NavMenuLinks,
  NavBtn,
  LightThemeIcon,
  DarkThemeIcon,
} from "./NavbarElements";
import { menuData } from "../data/MenuData";
import { Button } from "../elements/Button";
import { useModal } from "../../context/modalContext";
import { useSectionRef } from "../../context/InfoSectionContext";
import { useDarkMode } from "../../context/darkModeContext";
import MobileMenu from "../MobileMenu";

const Navbar = () => {
  const [navbarColor, setNavbarColor] = useState(false);
  const { modalToggle } = useModal();
  const myRef = useSectionRef();
  const { setTheme } = useDarkMode();
  const { darkMode } = useDarkMode();

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const changeBg = () =>
    window.scrollY >= window.innerHeight
      ? setNavbarColor(true)
      : setNavbarColor(false);

  useEffect(() => {
    window.addEventListener("scroll", changeBg);
    return () => {
      window.removeEventListener("scroll", changeBg);
    };
  });

  useEffect(() => {
    localStorage.setItem("theme", darkMode ? "night" : "day");
  }, [darkMode]);
  return (
    <Nav active={navbarColor}>
      <Logo to="/">INCRE</Logo>
      {darkMode ? (
        <LightThemeIcon onClick={setTheme} />
      ) : (
        <DarkThemeIcon onClick={setTheme} />
      )}
      <MenuBars onClick={toggle} />
      {isOpen ? <MobileMenu isOpen={isOpen} toggle={toggle} /> : null}
      <NavMenu>
        {menuData.map(function (item, index) {
          return myRef.refer.current && index == 0 ? (
            <NavMenuLinks
              to={item.link}
              key={index}
              onClick={() => myRef.refer.current.scrollIntoView()}>
              {item.title}
            </NavMenuLinks>
          ) : (
            <NavMenuLinks to={item.link} key={index}>
              {item.title}
            </NavMenuLinks>
          );
        })}
      </NavMenu>
      <NavBtn>
        <Button to="#" onClick={modalToggle}>
          Contact
        </Button>
      </NavBtn>
    </Nav>
  );
};

export default Navbar;
