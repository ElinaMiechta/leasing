import React, { useState } from "react";
import { useModal } from "../../context/modalContext";
import { ImCross } from "react-icons/im";
import { AiOutlineCheck } from "react-icons/ai";
import "./index.css";
import { ModalSuccess } from "../ModalSuccess";
import useForm from "../../hooks/useForm";
import { validate } from "../elements/validate";

const ModalContact = () => {
  const { handleChange, data, handleSubmit, errors, isSubmitted } = useForm(
    validate
  );
  const modal = useModal();
  const [checked, setChecked] = useState(false);
  const { modalToggle } = useModal();

  const toggle = () => setChecked(prev => !prev);

  const checkedIcon = {
    margin: "0 auto",
    fill: "#CD853F"
  };

  return (
    <>
      {modal.visible ? (
        <div className="modal">
          <div className="modal-exit" onClick={modalToggle}>
            <ImCross className="cross-icon"/>
          </div>
          <div className="modal-content">
            <div className="modal-left"></div>
            <div className="modal-right">
              {isSubmitted ? (
                <ModalSuccess />
              ) : (
                <form className="form" onSubmit={handleSubmit}>
                  <label htmlFor="name">Your first name</label>
                  <input
                    type="text"
                    placeholder="Enter your name"
                    id="name"
                    name="name"
                    value={data.name}
                    onChange={handleChange}
                  />
                  {errors.name && (
                    <span className="info red">{errors.name}</span>
                  )}
                  <label htmlFor="email">Your email</label>
                  <input
                    type="email"
                    placeholder="Enter your email"
                    id="email"
                    name="email"
                    value={data.email}
                    onChange={handleChange}
                  />
                  {errors.email && (
                    <span className="info red">{errors.email}</span>
                  )}
                  <label htmlFor="phone">Your phone</label>
                  <input
                    type="text"
                    placeholder="1 223 455 06"
                    id="phone"
                    name="phone"
                    value={data.phone}
                    onChange={handleChange}
                  />
                  {errors.phone && (
                    <span className="info red">{errors.phone}</span>
                  )}
                  <label htmlFor="msg">Your enquiry</label>
                  <textarea
                    id="msg"
                    name="msg"
                    value={data.msg}
                    onChange={handleChange}></textarea>
                  <label id="check">
                    Keep me informed of the latest motor industry news, new
                    vehicle launches, changes to legislation and special offers.
                    See our privacy policy.
                  </label>

                  <div className="form-checkbox__check" onClick={toggle}>
                    {checked && <AiOutlineCheck style={checkedIcon} />}
                  </div>

                  <div className="form-btton__wrapper">
                    <button>Send enquiry</button>
                  </div>
                </form>
              )}
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
};
export default ModalContact;
