import CountrySide from '../../images/cars/car8.jpg';
import Car1 from '../../images/cars/car7.jpg';
import Car2 from '../../images/cars/car1.jpg';
import Car3 from '../../images/cars/car2.jpg';
import Car4 from '../../images/cars/car3.jpg';

export const SliderData = [
  {
    title: "Dream worthy cars for leasing. Get Yours Honda",
    price: "$47 200",
    path: "/cars",
    label: "View Cars",
    image: CountrySide,
    alt: "cars"
  },
  {
    title: "Dream worthy cars for leasing. Discover Mercedes",
    price: "$55 000",
    path: "/cars",
    label: "View Cars",
    image: Car1,
    alt: "cars"
  },
  {
    title: "Dream worthy cars for leasing. Modern, fast, luxury",
    price: "$42 900",
    path: "/cars",
    label: "View Cars",
    image: Car2,
    alt: "cars"
  },
  {
    title: "Dream worthy cars for leasing. Let Yourself enjoy driving ",
    price: "$60 000",
    path: "/cars",
    label: "View Cars",
    image: Car3,
    alt: "cars"
  },
  {
    title: "Dream worthy cars for leasing. Serious desicisions",
    price: "$47 200",
    path: "/cars",
    label: "View Cars",
    image: Car4,
    alt: "cars"
  }
];
