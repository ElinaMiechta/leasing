export const generalData = [
  {
    title: "Service Center",
    dataText:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim convallis aenean et tortor at risus. "
  },
  {
    title: "Assistance",
    dataText:
      "Eget gravida cum sociis natoque penatibus. Senectus et netus et malesuada fames ac turpis egestas."
  },
  {
    title: "Car change",
    dataText:
      "Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus. In hac habitasse platea dictumst vestibulum rhoncus est."
  },
  {
    title: "Fuel politics",
    dataText:
      "Pulvinar elementum integer enim neque volutpat. Sollicitudin nibh sit amet commodo. Sed augue lacus viverra vitae. Eros in cursus turpis massa. Egestas integer eget aliquet nibh praesent tristique magna."
  },
  {
    title: "Application support",
    dataText:
      "Risus quis varius quam quisque id diam vel quam elementum. Pretium viverra suspendisse potenti nullam ac tortor vitae purus. Pellentesque adipiscing commodo elit at. Fringilla urna porttitor rhoncus dolor purus non enim praesent elementum. "
  },
  {
    title: "Finanses",
    dataText:
      "Odio eu feugiat pretium nibh ipsum. Morbi leo urna molestie at elementum eu facilisis. Adipiscing diam donec adipiscing tristique risus nec feugiat. Fermentum odio eu feugiat pretium nibh ipsum. Facilisis magna etiam tempor orci eu lobortis. Massa vitae tortor condimentum lacinia quis vel eros."
  },
  {
    title: "Insuarence",
    dataText:
      "Phasellus vestibulum lorem sed risus. Turpis cursus in hac habitasse platea dictumst quisque sagittis. Adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus. Erat imperdiet sed euismod nisi porta lorem mollis. Feugiat in ante metus dictum at. "
  }
];
