import Two from '../../images/cars/car4.jpg'
import One from '../../images/cars/car9.jpg'
import Three from '../../images/cars/car12.jpg'
import Four from '../../images/cars/car13.jpg'


export const InfoSectionData = [{
  heading: "Explore your brand new personality features",
  p1:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
  p2:
    "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    btnLabel: 'View Cars',
    image: One,
    reverse: false,
    left: false,
    delay: 100
},
{
     heading: "All modern premiers, best of their class",
      p1:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      p2:
        "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        btnLabel: 'View Cars',
        image: Two,
        reverse: true,
        left: true,
        delay: 300
}
];

export const InfoSectionDataOffer = 
{
  
  heading: "Safe and clear agreement, office in the Capital city",
   p1:
     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
   p2:
     "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
     btnLabel: 'View Cars',
     image: Three,
     reverse: true,
     left: true,
     delay: 300

};
export const InfoSectionDataOfferDark = {
  heading: "Our experience counts 18 years. We have strong Partners and stable Clients",
   p1:
     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
   p2:
     "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
     btnLabel: 'View Cars',
     image: Four,
     reverse: false,
     left: false,
     delay: 0,
     bg:true
     

}





