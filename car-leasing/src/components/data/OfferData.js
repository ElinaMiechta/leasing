import One from '../../images/cars/car11.jpg';
import Two from '../../images/cars/car10.jpg';

export const OfferData = [
      {
            image: One,
            alt: 'car',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
            label: 'View Details',
            top: '7rem',
            left: '-1%',
            animation: 'toLeft'
      },
      {
            image: Two,
            alt: 'car',
            desc: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore en.',
            label: 'View Details',
            top: '27rem',
            left: '50%',
            animation: 'toRight'
      }
]