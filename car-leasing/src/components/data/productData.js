export const productData = [
  {
    id: "1",
    img:
      "https://images.unsplash.com/photo-1546518071-fddcdda7580a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80",
    "img-ed":
      "https://sm.askmen.com/askmen_in/articlepage/2/2014-mercedes-benz-s-class/2014-mercedes-benz-s-class_3t38.jpg",
    "img-s1":
      "https://s.wsj.net/public/resources/images/TR-AA072_CARS_P_20160914144625.jpg",
    "img-s2":
      "https://www.goodcarbadcar.net/wp-content/uploads/2020/03/Mercedes-Benz-C-Class-1.jpg",
    altimg: "Mercedes AMG 2020",
    mark: "Mercedes",
    model: "lorem",
    price: "from 120 000$",
  },
  {
    id: "2",
    img:
      "https://images.unsplash.com/photo-1542230387-bfc77d26903e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1234&q=80",
    "img-ed":
      "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/large-2479-s-classsaloon.jpg?itok=QTxMln2k",
    "img-s1":
      "https://s.wsj.net/public/resources/images/TR-AA072_CARS_P_20160914144625.jpg",
    "img-s2":
      "https://www.goodcarbadcar.net/wp-content/uploads/2020/03/Mercedes-Benz-C-Class-1.jpg",
    altimg: "Mercedes AMG 2020",
    mark: "Mercedes",
    model: "lorem",
    price: "from 120 000$",
  },
  {
    id: "3",
    img:
      "https://images.unsplash.com/photo-1555652736-e92021d28a10?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
    "img-ed":
      "https://cars.usnews.com/images/article/202012/128773/2021_Audi_A8_00.jpg",
    "img-s1": "https://i.artfile.ru/2880x1800_1483395_[www.ArtFile.ru].jpg",
    "img-s2": "https://img5.goodfon.ru/wallpaper/nbig/e/3a/audi-q8-audi-q8.jpg",
    altimg: "Audi",
    mark: "Audi",
    model: "lorem",
    price: "from 100 000$",
  },
  {
    id: "4",
    img:
      "https://images.unsplash.com/photo-1580128233802-692c1fb65e76?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    "img-ed":
      "https://cars.usnews.com/images/article/202012/128773/2021_Audi_A8_00.jpg",
    "img-s1": "https://i.artfile.ru/2880x1800_1483395_[www.ArtFile.ru].jpg",
    "img-s2": "https://img5.goodfon.ru/wallpaper/nbig/e/3a/audi-q8-audi-q8.jpg",
    altimg: "Audi 2020",
    mark: "Audi",
    model: "lorem",
    price: "from 110 000$",
  },
  {
    id: "5",
    img:
      "https://images.unsplash.com/photo-1561580125-028ee3bd62eb?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
    "img-ed":
      "https://cdn.shopify.com/s/files/1/0196/5170/files/Screen_Shot_2018-04-14_at_11.23.10_AM_grande.png?v=1523719447",
    "img-s1":
      "https://cdn.motor1.com/images/mgl/PKJN2/s3/tesla-model-s-has-highest-resale-value-among-luxury-cars-in-germany.jpg",
    "img-s2":
      "https://www.cdn.tv2.no/images/9661239.jpg?imageId=9661239&panow=100&panoh=46.666666666667&panox=0&panoy=40.47619047619&heightw=0&heighth=0&heightx=0&heighty=0&width=600&height=315",
    altimg: "Tesla",
    mark: "Tesla",
    model: "lorem",
    price: "from 150 000$",
  },
  {
    id: "6",
    img:
      "https://images.unsplash.com/photo-1585011664466-b7bbe92f34ef?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    "img-ed":
      "https://cdn.shopify.com/s/files/1/0196/5170/files/Screen_Shot_2018-04-14_at_11.23.10_AM_grande.png?v=1523719447",
    "img-s1":
      "https://cdn.motor1.com/images/mgl/PKJN2/s3/tesla-model-s-has-highest-resale-value-among-luxury-cars-in-germany.jpg",
    "img-s2":
      "https://www.cdn.tv2.no/images/9661239.jpg?imageId=9661239&panow=100&panoh=46.666666666667&panox=0&panoy=40.47619047619&heightw=0&heighth=0&heightx=0&heighty=0&width=600&height=315",
    altimg: "Tesla",
    mark: "Tesla",
    model: "lorem",
    price: "from 95 000$",
  },
  {
    id: "7",
    img:
      "https://images.unsplash.com/photo-1610908374865-dae3c6392a2f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=675&q=80",
    "img-ed":
      "https://www.supercars.net/blog/wp-content/uploads/2017/08/New-Continental-GT-105.jpg",
    "img-s1":
      "https://besthqwallpapers.com/Uploads/17-9-2020/141465/thumb2-bentley-continental-gt-mulliner-2020-4k-front-view-exterior.jpg",
    "img-s2":
      "http://s1.cdn.autoevolution.com/images/news/gallery/2015-bentley-continental-gt-updated-ahead-geneva-debut-video-photo-gallery_1.jpg",
    altimg: "Bentley",
    mark: "Bentley",
    model: "lorem",
    price: "from 120 000$",
  },
  {
    id: "8",
    img:
      "https://images.unsplash.com/photo-1576668273906-4c087ac1dc85?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    "img-ed":
      "https://www.supercars.net/blog/wp-content/uploads/2017/08/New-Continental-GT-105.jpg",
    "img-s1":
      "https://besthqwallpapers.com/Uploads/17-9-2020/141465/thumb2-bentley-continental-gt-mulliner-2020-4k-front-view-exterior.jpg",
    "img-s2":
      "http://s1.cdn.autoevolution.com/images/news/gallery/2015-bentley-continental-gt-updated-ahead-geneva-debut-video-photo-gallery_1.jpg",
    altimg: "Bentley",
    mark: "Bentley",
    model: "ipsum lorem quadro",
    price: "from 139 100$",
  },
  {
    id: "9",
    img:
      "https://images.unsplash.com/photo-1541800658-6599fffd81c1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=831&q=80",
    "img-ed": "https://gtspirit.com/wp-content/uploads/2015/02/Audi.jpg",
    "img-s1": "https://i.artfile.ru/2880x1800_1483395_[www.ArtFile.ru].jpg",
    "img-s2": "https://img5.goodfon.ru/wallpaper/nbig/e/3a/audi-q8-audi-q8.jpg",
    altimg: "Audi",
    mark: "Audi",
    model: "ipsum 3000",
    price: "from 99 999$",
  },
  {
    id: "10",
    img:
      "https://images.unsplash.com/photo-1605556816125-d752c226247b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    "img-ed":
      "https://cnet3.cbsistatic.com/img/CpLiLwHJCsQ6G5z7L0vgUxpefVY=/940x0/2019/10/21/fb72aa13-6e23-48cc-9dd2-5ef62f097365/2020-mercedes-amg-gt-007.jpg",
    "img-s1":
      "https://s.wsj.net/public/resources/images/TR-AA072_CARS_P_20160914144625.jpg",
    "img-s2":
      "https://www.goodcarbadcar.net/wp-content/uploads/2020/03/Mercedes-Benz-C-Class-1.jpg",
    altimg: "Mercedes",
    mark: "Mercedes",
    model: "AMG lorem",
    price: "from 54 000$",
  },
  {
    id: "11",
    img:
      "https://images.unsplash.com/photo-1541878117466-0e3000a65864?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    "img-ed": "https://i.ytimg.com/vi/MHYzVIhhtrk/hqdefault.jpg",
    "img-s1":
      "https://img.autobytel.com/2021/toyota/avalon/2-800-oem-exterior-front1300-95745.jpg",
    "img-s2":
      "https://tanishtaxiservice.advertroindia.co.in/uploads-advertro-03-08-2019/Tanishtaxiservice/products/25310/toyo.JPG",
    altimg: "Toyota",
    mark: "Toyota",
    model: "quatro lorem",
    price: "from 90 000$",
  },
  {
    id: "12",
    img:
      "https://images.unsplash.com/flagged/photo-1553505192-acca7d4509be?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1067&q=80",
    "img-ed":
      "https://pnevmo-podveska.com/wp-content/uploads/2016/02/BMW_i8_s_pnevmopodveskoy_03-600x370.jpg",
    "img-s1":
      "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/body-image/public/1-bmw-7-series-730ld-2019-uk-fd-hero-front.jpg?itok=3hIdcBPu",
    "img-s2":
      "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/body-image/public/bmw-6-series-gran-turismo_2.jpg?itok=SnibLzLU",
    altimg: "BMW",
    mark: "BMW",
    model: "quatro lorem",
    price: "from 100 000$",
  },
  {
    id: "13",
    img:
      "https://images.unsplash.com/photo-1593460354583-4224ab273cfe?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=966&q=80",
    "img-ed":
      "https://images.cars.com/cldstatic/wp-content/uploads/img-199896234-1478891897352.jpg",
    "img-s1":
      "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/body-image/public/1-bmw-7-series-730ld-2019-uk-fd-hero-front.jpg?itok=3hIdcBPu",
    "img-s2":
      "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/body-image/public/bmw-6-series-gran-turismo_2.jpg?itok=SnibLzLU",
    altimg: "BMW",
    mark: "BMW",
    model: "quatro lorem",
    price: "from 119 200$",
  },
];
