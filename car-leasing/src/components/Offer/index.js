import {
  OfferSection,
  OfferContainer,
  Title,
  TitleDescription,
  Arrow,
  OfferLink,
  OverlayContainer,
} from "./OfferElements";
import { useDarkMode } from "../../context/darkModeContext";
import { useAnimation } from "../../hooks/useAnimation";

const Offer = ({ offers, onClick }) => {
  const { activate, animationManage } = useAnimation(1300);
  const { darkMode } = useDarkMode();

  window.addEventListener("scroll", () => animationManage());
  window.removeEventListener('scroll', animationManage);

  return (
    <OfferSection darkMode={darkMode}>
      <Title darkMode={darkMode}>View our newest Cars</Title>

      {offers.map((offer, index) => {
        return (
          <OfferContainer
            key={index}
            top={offer.top}
            left={offer.left}
            animation={offer.animation}
            activate={activate}
            darkMode={darkMode}>
            <img src={offer.image} alt={offer.alt} />
            <OverlayContainer darkMode={darkMode}>
              <p>Price from $100 000</p>
              <p>
                Contact US and our managers will provide all necessary
                information
              </p>
            </OverlayContainer>
            <TitleDescription darkMode={darkMode}>
              {offer.desc}
            </TitleDescription>
            <OfferLink onClick={onClick} darkMode={darkMode}>
              {offer.label}
              <Arrow darkMode={darkMode} />
            </OfferLink>
          </OfferContainer>
        );
      })}
    </OfferSection>
  );
};

export default Offer;
