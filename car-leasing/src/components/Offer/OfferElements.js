import styled, { keyframes } from "styled-components/macro";
import { IoMdArrowRoundForward } from "react-icons/io";

const appear = keyframes`
  from {
    opacity:0.1
  }

  to {
   opacity: 1
  }
`;

export const OfferSection = styled.section`
  width: 100%;
  min-height: 100%;
  position: relative;
  padding: 4rem 0rem;
  background-color: ${({darkMode}) => (darkMode ? '#0b0b15' : '#fff')}
`;

export const Title = styled.h1`
  font-size: clamp(2rem, 4vw, 1.5rem);
  margin-left: 4rem;
  color: ${({ darkMode }) => (darkMode ? '#fff' : '#000')};
`;
export const OfferContainer = styled.div`
  padding: 3rem calc((100vw - 1300px) / 2);
  display: flex;
  width: 50%;
  height: 80vh;
  flex-direction: column;
  position: absolute;
  top: ${props => props.top};
  left: ${props => props.left};
  background-color: ${({darkMode}) => (darkMode ? '#0b0b15' : '#fff')};
  transition: 2s ease-in-out;
  animation: ${({ activate }) => (activate ? appear : "none")} 2s linear 1;

  &:hover > div {
    transition: 0.5s ease-in-out;
    opacity: 0.8;
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    transition: 0.5s ease-in-out;

    &:hover {
      filter: brightness(0.8);
    }
  }

  @media only screen and (max-width: 1024px) {
    width: 100%;
    left: 0;
    top: ${({ top }) => (top === "27rem" ? "49rem" : "6rem")};
  }

  @media only screen and (max-width: 768px) {
    margin: 0rem;
    margin-top: 3rem;
  }

  @media only screen and (max-width: 375px) {
  margin-top: 9rem;
  }
`;

export const OverlayContainer = styled.div`
  background: linear-gradient(90deg, #dfede3, #f0f5f1);
  width: 100%;
  height: 40%;
  opacity: 0;
  margin-top: -10.5rem;
  transition: 0.5s ease-in-out;

  p {
    color: #242424;
    padding: 1rem 2rem;
    font-size: 16px;
    font-weight: 700;
  }
`;

export const TitleDescription = styled.h2`
  font-weight: 400;
  padding: 2rem;
  color: ${({ darkMode }) => (darkMode ? '#fff' : '#000')};
`;

export const OfferLink = styled.p`
  font-weight: 700;
  padding: 2rem;
  display: inline;
  margin-top: -40px;
  cursor: pointer;
  user-select: none;
  transition: 0.5s ease-in-out;
  color: ${({ darkMode }) => (darkMode ? '#fff' : '#000')};

  &:hover {
    transform: translateY(-6px);
  }
`;
export const Arrow = styled(IoMdArrowRoundForward)`
  margin-left: 1.5rem;
  color: ${({ darkMode }) => (darkMode ? '#fff' : '#000')};
`;
