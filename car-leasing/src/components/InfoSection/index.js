import {
  Section,
  Container,
  ColumnLeft,
  ColumnRight,
} from "./InfoSectionElements";
import { Button } from "../elements/Button";
import { useDarkMode } from "../../context/darkModeContext";
import { useAnimation } from "../../hooks/useAnimation";

const InfoSection = ({ infoData }) => {
  const { activate, animationManage } = useAnimation(150);
  const animateLeftSide = useAnimation(3310);
  const animateRightSide = useAnimation(3104);

  const { darkMode } = useDarkMode();
  window.addEventListener("scroll", () => {
    animationManage();
    animateLeftSide.animationManage();
    animateRightSide.animationManage();
  });

  return (
    <Section darkMode={darkMode} activate={activate}>
      {infoData.length > 1 ? (
        infoData.map((data, index) => (
          <Container key={index}>
            <ColumnLeft darkMode={darkMode}>
              <h1>{data.heading}</h1>
              <p>{data.p1}</p>
              <p>{data.p2}</p>
              <Button to="/cars" darkMode={darkMode}>
                {data.btnLabel}
              </Button>
            </ColumnLeft>
            <ColumnRight
              reverse={data.reverse}
              left={data.left}
              delay={infoData.delay}>
              <img src={data.image} alt="car" />
            </ColumnRight>
          </Container>
        ))
      ) : (
        <Container bg={infoData.bg}>
          <ColumnLeft darkMode={darkMode} activate={animateLeftSide.activate}>
            <h1>{infoData.heading}</h1>
            <p>{infoData.p1}</p>
            <p>{infoData.p2}</p>
            <Button to="/cars" darkMode={darkMode}>
              {infoData.btnLabel}
            </Button>
          </ColumnLeft>
          <ColumnRight
            reverse={infoData.reverse}
            left={infoData.left}
            delay={infoData.delay}
            activate={animateRightSide.activate}>
            <img src={infoData.image} alt="car" />
          </ColumnRight>
        </Container>
      )}
    </Section>
  );
};

export default InfoSection;
