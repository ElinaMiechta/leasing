import styled, {keyframes} from "styled-components/macro";


const sectionAnimation = keyframes`
0% {
  opacity:0;
}

50% {
  opacity:0.5
}
100% {
  opacity:1;
}
`;

const sectionFadeOutAnimation = keyframes`
from {
  opacity:1;
}

to {
  opacity:0;
}
`;

const columnLeftAnimation = keyframes`
from {
  transform:translateX(100%);
}

to {
  transform: translateX(0%);
}
`;

const columnRightAnimation = keyframes`
from {
  transform:translateX(-100%);
}

to {
  transform: translateX(0%);
}
`;

export const Section = styled.section`
  width: 100%;
  height: 100%;
  padding: 4rem 0rem;
  background-color: ${({ darkMode }) => (darkMode ? "#0b0b15" : "#fff")};
  background: ${props => (props.bg ? "#0b0b15" : "transparent")};
  animation: ${({ activate }) => (activate ? sectionAnimation : sectionFadeOutAnimation)} 2s linear 1;
`;

export const Container = styled.div`
  padding: 3rem calc((100vw - 1300px) / 2);
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 800px;
  transition: 1s ease-out;
  background: ${({ bg }) => (bg ? "#0b0b15" : "transparent")};

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

export const ColumnRight = styled.div`
  padding: 1rem 2rem;
  order: ${({ reverse }) => (reverse ? "1" : "2")};
  display: flex;
  justify-content: center;
  align-items: center;
  transition: 2s ease-out;
  animation: ${({ activate }) => (activate ? columnRightAnimation : "none")} 1s linear 1;

  @media screen and (max-width: 768px) {
    order: ${({ reverse }) => (reverse ? "2" : "1")};
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;

    @media screen and (max-width: 768px) {
      width: 90%;
      height: 90%;
    }
  }
`;

export const ColumnLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  line-height: 1.4;
  padding: 1rem 2rem;
  order: ${({ reverse }) => (reverse ? "1" : "2")};
  animation: ${({ activate }) => (activate ? columnLeftAnimation : "none")} 1s linear 1;

  h1 {
    margin-bottom: 1rem;
    font-size: clamp(1.5rem, 6vw, 2rem);
    color: ${({ darkMode }) => (darkMode ? "#fff" : "#000")};
  }

  p {
    margin-bottom: 2rem;
    color: ${({ darkMode }) => (darkMode ? "#fff" : "#000")};
  }

  a {
    z-index: 1;
  }
`;
