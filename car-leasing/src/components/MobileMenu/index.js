import React from "react";
import {
  DropDownContainer,
  Icon,
  CloseIcon,
  DropDownWrapper,
  MobileMenuDropdown,
  BtnWrap,
  MobileMenuLink
} from "./MobileMenuElements";
import { menuData } from "../data/MenuData";
import { Button } from "../elements/Button";
import {useModal} from '../../context/modalContext'

const MobileMenu = ({ isOpen, toggle }) => {
  const { modalToggle } = useModal();
  return (
    <DropDownContainer isOpen={isOpen} onClick={toggle}>
      <Icon onClick={toggle}>
        <CloseIcon />
      </Icon>
      <DropDownWrapper>
        <MobileMenuDropdown>
          {menuData.map((item, index) => (
            <MobileMenuLink to={item.link} key={index}>
              {item.title}
            </MobileMenuLink>
          ))}
        </MobileMenuDropdown>
        <BtnWrap>
          <Button primary="true" round="true" big="true" onClick={modalToggle}>
            Contact Us
          </Button>
        </BtnWrap>
      </DropDownWrapper>
    </DropDownContainer>
  );
};

export default MobileMenu;
