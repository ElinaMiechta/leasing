import FooterDataElement from './FooterDataElement';

const FooterElement = ({ data }) => {
      return (
        <>
          {data.map((elem, index) => (
            <>
              <div className="col-4" key={index}>
                <FooterDataElement title={elem.title} listElement={elem.elemList} key={elem.title}/>
              </div>
            </>
          ))}
        </>
      );
    };

export default FooterElement;