import './index.css';

const FooterDataElement = ({title, listElement}) => {
return (
      <div className="data-col">
      <p className="title">{title}</p>
      <ul className="data-list">
            {listElement.map((el, index) => (
                    <li key={index}>{el}</li>
            ))}
      </ul>
      </div>
)
}

export default FooterDataElement;