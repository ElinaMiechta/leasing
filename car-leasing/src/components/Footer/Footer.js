import "./index.css";
import { CgArrowUpO } from "react-icons/cg";
import { IoLogoYoutube } from "react-icons/io";
import { AiFillFacebook, AiFillInstagram } from "react-icons/ai";
import FooterElement from "./FooterElement";
import {
  footerColBuy,
  footerColDeals,
  footerColWork
} from "../data/FooterData";

const Footer = () => {
  return (
    <>
      <footer className="footer">
        <div className="col-12">
          <CgArrowUpO
            className="arr--up"
            onClick={() => window.scrollTo(0, 0)}
          />
        </div>
        <div className="footer-content">
          <FooterElement data={footerColBuy} />
          <FooterElement data={footerColDeals} />
          <FooterElement data={footerColWork} />
        </div>
        <div className="divider"></div>
        <div className="sm-content">
          <div className="col-8">
            <span>
              &#169; Car Leasing <span id="primary">INCRE</span>
            </span>
            <span>Cookies</span>
            <span>Rights</span>
          </div>
          <div className="col-8">
            <IoLogoYoutube className="sm__icon" />
            <AiFillFacebook className="sm__icon" />
            <AiFillInstagram className="sm__icon" />
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
