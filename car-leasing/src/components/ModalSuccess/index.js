import '../ModalContact/index.css';
export const ModalSuccess = () => {

      return <div className="modal-success">
            <p className="success-text">Thank you!</p>
            <p>Soon we will contact You.</p>
            <div className="icon"></div>
      </div>
}