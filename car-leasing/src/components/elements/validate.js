export const validate = data => {
  let errors = {};

  if (!data.name.trim()) {
    errors.name = "Your name is required";
  }

  if (!data.email) {
    errors.email = "Email is required";
  } else if (!/\S+@\S+\.\S+/.test(data.email)) {
    errors.email = "Email is not valid";
  }

  if (!data.phone) {
    errors.phone = "Phone is mising";
  } else if (!/^(\+)?([ 0-9]){9,16}$/g.test(data.phone)) {
    errors.phone = "Phone number is not correct";
  }

  return errors;
};
