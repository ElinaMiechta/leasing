import styled, { css } from "styled-components/macro";
import { AiOutlineLine } from "react-icons/ai";
import {SiMercedes, SiAudi, SiBmw, SiTesla, SiBentley, SiToyota} from 'react-icons/si'

const flexBox = css`
display:flex;
flex-direction:column;
align-items: center;
justify-content: center;
`;

const fullSize = css`
width:100%;
height:100%;
`;

export const Container = styled.div`
${fullSize}
display:flex;
align-items:center
box-sizing: border-box;
flex-direction:column;
`;

export const ParallaxContainer = styled.div`
width: 100%;
height: 100vh;
display:flex;
align-items:center;
position: relative;
background-image: url('${props => props.bgSrc}');
background-repeat: no-repeat;
background-size: cover;

background-attachment: fixed;
background-position: center;
background-repeat: no-repeat;
background-size: cover;
`;

export const CarHeroBackground = styled.div`
${fullSize}
  position: absolute;
  top: 0;
  left: 0;
`;

export const CarHeroForeground = styled.div`
  margin-top: auto;
  width: 100%;
  height: 30%;
  display: flex;
  align-items: center;
`;

export const CarBgContainer = styled.div`
  width: 100%;
  background-color: #00000085;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
  z-index: 1;
`;

export const MainHeader = styled.h1`
  font-size: clamp(1.5rem, 4vw, 4rem);
  color: #fff;
  margin: 1rem;
`;

export const SubHeader = styled.h2`
  color: #fff;
  font-size: clamp(1rem, 2vw, 2rem);
  margin: 0px 20px;
`;

export const Liner = styled(AiOutlineLine)`
fill: #CD853F;
font-size: 5rem;
`;

export const CarModelWrapper = styled.div`
  width: 100%;
  max-height: 400px;
  min-height:300px;
  ${flexBox}
`;

export const Models = styled.div`
width: 70%;
height:100%;
margin: 0 auto;
box-sizing: border-box;
display:flex;
align-items:center;

@media only screen and (max-width: 1012px) {
  width:100%;
  flex-wrap: wrap;
}
@media only screen and (max-width: 754px) {
justify-content: center;
}
`;

const icons = css`
font-size: 8rem;
margin: 0 2rem;
color: ${props => props.dark ? '#8c8484' : '#232323'};
transition: .5s ease-in;

&:hover {
    color:#581717;
    transform: scale(1.1)
}
@media only screen and (max-width: 986px) {
margin: 3rem 2rem;
}

@media only screen and (max-width: 620px) {
  font-size: 4em;
}
`;

export const Mercedes = styled(SiMercedes)`
${icons}
`;

export const BMW = styled(SiBmw)`
${icons}
`;

export const Audi = styled(SiAudi)`
${icons}
`;

export const Tesla = styled(SiTesla)`
${icons}
`;

export const Bentley = styled(SiBentley)`
${icons}
`;


export const Toyota = styled(SiToyota)`
${icons}
`;

export const CarsContainer = styled.div`
${flexBox}
${fullSize}

@media only screen and (max-width: 450px) {
  margin-top:2rem;
}
`;

export const CarsOffers = styled.div`
${flexBox}
flex-direction: row;
flex-wrap: wrap;
width:80%;
height:90%;

@media only screen and (max-width: 425px) {
  width: 100%;
}
`;

export const ModelTitle = styled.h3`
font-size: clamp(0.5rem, 4vw, 2rem);
color: ${props => props.dark ? '#fff' : '#000'};
margin: 2rem;
font-weight: 600;
cursor:pointer;
`;

export const OptionsContainer = styled.div`
height: 200px;
margin: 2rem;
margin-top: 5rem;
display:flex;
align-items: flex-start;
justify-content: flex-start;
flex-direction: column;

@media only screen and (max-width: 450px) {
  width:100%;
  margin-left: -1rem;
}
`;

export const OptionsHeader = styled.span`
font-size: clamp(1rem, 4vw, 2rem);
color: ${props => props.dark ? '#fff' : '#000'};
font-weight: 600;
text-transorm: uppercase;
margin: 2rem;
cursor: pointer;
`;

export const OptionGroup = styled.div`
${flexBox}
flex-direction: row;
justify-content: flex-start;
margin: 2rem;
box-shadow: 5px 5px 10px #232323c9;

@media only screen and (max-width: 748px) {
  box-shadow: none;
}

@media only screen and (max-width: 450px) {
  margin:0;
  margin-left: 23px;
}
`;

export const ButtonsContainer = styled.div`
${fullSize}
background-color: ${props => props.dark ? '#8c8484' : 'transparent'}
`;

const btnOpt = css`
border: 2px solid ${props => props.dark ? '#CD853F' : '#0b0b15'};
border-right-width: 1px;
background-color: ${props => (props.active ? "#10101f" : "transparent")};
color: ${props => (props.active ? "#fff" : "#10101f")};
padding: 8px 16px;
transition: .5s ease-in-out;

&:focus {
  outline: none;
}

&:hover {
background-color: #10101f;
color: #fff;
}

`;

export const AllOpt = styled.button`
${btnOpt}
`;
export const MercedesOpt = styled.button`
${btnOpt}
`;
export const BWMOpt = styled.button`
${btnOpt}
`;
export const AudiOpt = styled.button`
${btnOpt}
`;
export const TeslaOpt = styled.button`
${btnOpt}
`;
export const BentleyOpt = styled.button`
${btnOpt}
`;
export const ToyotaOpt = styled.button`
${btnOpt}
border-right-width: 2px;
`;

