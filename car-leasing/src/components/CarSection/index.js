import React, { useState, useRef } from "react";
import {
  Container,
  CarBgContainer,
  MainHeader,
  SubHeader,
  Liner,
  CarModelWrapper,
  Models,
  Mercedes,
  BMW,
  Audi,
  ParallaxContainer,
  CarHeroBackground,
  CarHeroForeground,
  Tesla,
  Bentley,
  Toyota,
  CarsContainer,
  CarsOffers,
  ModelTitle,
  OptionsContainer,
  OptionsHeader,
  OptionGroup,
  ButtonsContainer,
  AllOpt,
  MercedesOpt,
  BWMOpt,
  AudiOpt,
  TeslaOpt,
  BentleyOpt,
  ToyotaOpt,
} from "./CarSectionElements";
import Car from "../Car";
import "./index.css";
import { useDarkMode } from "../../context/darkModeContext";

function CarSection({ imgsrc }) {
  const [targetName, setTargetName] = useState("");
  const options = useRef(null);
  const { darkMode } = useDarkMode();

  const targetNameToggle = (e) => {
    setTargetName(e.target.name);
    const childOpts = options.current.childNodes;
    childOpts.forEach((el) => {
      return el.name !== e.target.name
        ? el.classList.remove("active")
        : el.classList.add("active");
    });
  };

  return (
    <Container>
      <ParallaxContainer bgSrc={imgsrc}>
        <CarHeroBackground></CarHeroBackground>
        <CarHeroForeground>
          <CarBgContainer>
            <MainHeader>Lorem Ipsum quante</MainHeader>
            <SubHeader>lorem ipsum dolores qunte fabro divo</SubHeader>
            <Liner />
          </CarBgContainer>
        </CarHeroForeground>
      </ParallaxContainer>

      <CarModelWrapper>
        <ModelTitle dark={darkMode}>We work with best</ModelTitle>
        <Models>
          <Mercedes dark={darkMode} />
          <BMW dark={darkMode} />
          <Audi dark={darkMode} />
          <Tesla dark={darkMode} />
          <Bentley dark={darkMode} />
          <Toyota dark={darkMode} />
        </Models>
      </CarModelWrapper>
      <OptionsContainer>
        <OptionsHeader dark={darkMode}>Ready offers</OptionsHeader>
        <OptionGroup>
          <ButtonsContainer ref={options} dark={darkMode}>
            <AllOpt
              active={true}
              name="All"
              onClick={(e) => targetNameToggle(e)}
              dark={darkMode}>
              All cars
            </AllOpt>
            <MercedesOpt
              active={false}
              name="Mercedes"
              onClick={(e) => targetNameToggle(e)}
              dark={darkMode}>
              Mercedes
            </MercedesOpt>
            <BWMOpt
              active={false}
              name="BMW"
              onClick={(e) => targetNameToggle(e)}
              dark={darkMode}>
              BMW
            </BWMOpt>
            <AudiOpt
              active={false}
              name="Audi"
              onClick={(e) => targetNameToggle(e)}
              dark={darkMode}>
              Audi
            </AudiOpt>
            <TeslaOpt
              active={false}
              name="Tesla"
              onClick={(e) => targetNameToggle(e)}
              dark={darkMode}>
              Tesla
            </TeslaOpt>
            <BentleyOpt
              active={false}
              name="Bentley"
              onClick={(e) => targetNameToggle(e)}
              dark={darkMode}>
              Bentley
            </BentleyOpt>
            <ToyotaOpt
              active={false}
              name="Toyota"
              onClick={(e) => targetNameToggle(e)}
              dark={darkMode}>
              Toyota
            </ToyotaOpt>
          </ButtonsContainer>
        </OptionGroup>
      </OptionsContainer>

      <CarsContainer>
        <CarsOffers>
          <Car targetName={targetName} />
        </CarsOffers>
      </CarsContainer>
    </Container>
  );
}

export default CarSection;
