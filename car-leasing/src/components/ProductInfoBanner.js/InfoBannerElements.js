import styled , {keyframes} from 'styled-components';
const slideTxtAnimation  = keyframes`
      from {
        transform: translateX(100%)
      }
    
      to {
        transform: translateX(0%);
      }
    }
    `;

export const BottomSection = styled.div`
width:100%;
background-color: #fff;
height:40vh;
display:flex;
align-items: center;
justify-content: flex-start;

@media only screen and (max-width: 620px) {
      align-items: flex-start;   
}

@media only screen and (max-width: 412px) {
     margin-top:2rem;
}
`;

export const BottomLeft = styled.div`
width:40%;
text-align: center;
`;

export const BottomRight = styled.div`
width:50%;
flex:1;
`;

export const BottomTitle = styled.p`
font-size: 26px;
font-weight:600;
font-family: 'Montserrat',sans-serif;
color: #232323;
padding: 30px;

@media only screen and (max-width: 412px) {
      padding: 18px;
 }
`;

export const BottomSmallTitle = styled.p`
font-size: 26px;
font-weight:400;
font-family: 'Montserrat',sans-serif;
color: #232323;

@media only screen and (max-width: 412px) {
      font-size: 20px;
 }
`;

export const RightText = styled.p`
font-size: 20px;
font-weight:400;
font-family: 'Montserrat',sans-serif;
color: #232323;
padding: 20px;
animation: ${({ activate }) => (activate ? slideTxtAnimation : "none")} 2s linear 1;

@media only screen and (max-width: 412px) {
     padding: 10px;
     font-size:14px; 
}
`;