import styled , {css,keyframes} from 'styled-components';
import {AiOutlineLine} from 'react-icons/ai';

const fullSize = css`
width:100%;
height:100%;
`;
const textAnimation = keyframes`
from {
      transform: translateX(100%)
}

to {
      transform:translateX(0%)
}
`;

const infoAnimation= keyframes`
from {
      transform: translateX(-100%)
}

to {
      transform:translateX(0%)
}
`;

const flex = css`
display:flex;
flex-direction:column;
justify-content: center;
align-items: center;
`;


export const ParallaxContainer = styled.div`
margin-top: ${props => props.top};
width: 100%;
height: ${props => props.height};
display:flex;
align-items:center;
position: relative;
background-image: url('${props => props.bgSrc}');
background-repeat: no-repeat;
background-size: cover;

background-attachment: fixed;
background-position: center;
background-repeat: no-repeat;
background-size: cover;
`;

export const CarHeroBackground = styled.div`
${fullSize}
  position: absolute;
  top: 0;
  left: 0;
`;

export const CarHeroForeground = styled.div`
  margin-top: auto;
  width: 100%;
  height: 30%;
  display: flex;
  align-items: center;
`;

export const CarBgContainer = styled.div`
  width: 100%;
  background-color: #00000085;
  height: 100%;
  ${flex};
  justify-content: flex-start;
  z-index: 1;
  text-align:center;
`;

export const Liner = styled(AiOutlineLine)`
fill: #CD853F;
font-size: 5rem;
`;

export const ProductHeader = styled.h1`
color: #fff;
font-weight: 400;
font-size: clamp(2em, 6vw,3em);
animation: ${textAnimation} 2s  1;
`;

export const PropertiesContainer = styled.div`
${flex};
${fullSize}
flex-direction:row;
animation: ${({ activate }) => (activate ? textAnimation : "none")} 2s linear 1;

@media only screen and (max-width: 748px) {
      max-width:100%;
}
`;

export const InfoBgElement = styled.span`
color: #fff;
font-weight: 600;
font-size: 20px;
padding: 20px 80px;
animation: ${infoAnimation} 2s  1;

@media only screen and (max-width: 748px) {
      padding: 20px;
}

@media only screen and (max-width: 348px) {
      padding: 6px;
      font-size: 16px;
}
`;