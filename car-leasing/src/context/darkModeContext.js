import React, { useState, useContext } from "react";

const DarkModeContext = React.createContext(true);

export const useDarkMode = () => useContext(DarkModeContext);

export const DarkModeProvider = ({ children }) => {
  const [darkTheme, setDarkTheme] = useState(
    localStorage.getItem("theme") === "night" ? true : false
  );

  const setTheme = () => {
    setDarkTheme(!darkTheme);
  };

  return (
    <DarkModeContext.Provider
      value={{
        darkMode: darkTheme,
        setTheme
      }}>
      {children}
    </DarkModeContext.Provider>
  );
};
