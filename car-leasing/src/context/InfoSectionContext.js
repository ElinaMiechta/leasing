import React, { useState, useContext, useRef } from "react";

const InfoSectionContext = React.createContext(true);

export const useSectionRef = () => useContext(InfoSectionContext);

export const InfoSectionProvider = ({ children }) => {
  const [myRef, setMyRef] = useState(null);
  const reference = useRef(myRef);
  const setRefValue = el => setMyRef(el);

  return (
    <InfoSectionContext.Provider
      value={{
        refer: reference,
        setRefValue
      }}>
      {children}
    </InfoSectionContext.Provider>
  );
};
