import React, {useState, useContext} from "react";

//private
const ModalContext = React.createContext(true);

export const useModal = () => useContext(ModalContext);

export const ModalProvider = ({ children }) => {
  const [modal, setModal] = useState(false);
  const modalToggle = () => setModal(!modal);

  return (
  <ModalContext.Provider value={{
        visible : modal,
        modalToggle
  }}>
        {children}
</ModalContext.Provider>
  )
};


