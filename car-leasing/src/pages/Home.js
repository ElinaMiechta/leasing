import React, { useEffect } from "react";
import { SliderData } from "../components/data/SliderData";
import { OfferData } from "../components/data/OfferData";
import Hero from "../components/Hero";
import InfoSection from "../components/InfoSection";
import ModalContact from "../components/ModalContact";
import Offer from "../components/Offer";
import {
  InfoSectionData,
  InfoSectionDataOffer,
  InfoSectionDataOfferDark,
} from "../components/data/InfoSectionData";
import { useSectionRef } from "../context/InfoSectionContext";
import { useDarkMode } from "../context/darkModeContext";
import GlobalStyle from "../globalStyles";
import Footer from "../components/Footer/Footer";
import FeaturedCar from "../components/FeaturedCar";
import { sliderData } from "../components/data/FeaturedSliderData";

const Home = ({}) => {
  const { darkMode } = useDarkMode();
  const sectionRef = useSectionRef();
  const { setRefValue } = useSectionRef();

  useEffect(() => {
    const elem = document.querySelector(".col-3");
    setRefValue(elem);
  }, []);

  return (
    <>
      <GlobalStyle darkMode={darkMode} />
      <Hero slides={SliderData} />
      <InfoSection infoData={InfoSectionData} />
      <Offer offers={OfferData} />
      <div className="col-6">
        <InfoSection infoData={InfoSectionDataOffer} />
      </div>
      <div className="col-3" ref={sectionRef.refer}>
        <InfoSection infoData={InfoSectionDataOfferDark} bg={true} />
      </div>
      <div className="col-9">
        <span className="featured-title">Special offer </span>
        <FeaturedCar slides={sliderData} />
      </div>
      <div className="col-custom">
        <Footer />
      </div>
      <ModalContact />
    </>
  );
};

export default Home;
