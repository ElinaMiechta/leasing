import GlobalStyle from "../globalStyles";
import { useDarkMode } from "../context/darkModeContext";
import ProductCard from "../components/ProductCard";
import Footer from '../components/Footer/Footer';
import ModalContact from "../components/ModalContact";

const Product = () => {
const { darkMode } = useDarkMode();


//TODO:  add tests
return(
      <>
       <GlobalStyle darkMode={darkMode} />
       <ProductCard dark={darkMode}/>
       <Footer />
       <ModalContact/>
      </>
)
};

export default Product;