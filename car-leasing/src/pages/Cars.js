import CarSection from "../components/CarSection";
import GlobalStyle from "../globalStyles";
import ModalContact from "../components/ModalContact";
import Footer from "../components/Footer/Footer";
import FeaturedCar from "../components/FeaturedCar";
import { sliderData } from "../components/data/FeaturedSliderData";
import { useDarkMode } from "../context/darkModeContext";

const Cars = () => {
  const { darkMode } = useDarkMode();

  return (
    <>
      <GlobalStyle darkMode={darkMode} />
      <CarSection imgsrc={"/bg/bg-car.jpg"} />
      <div className="col-9__cars">
        <FeaturedCar slides={sliderData} />
      </div>
      <div className="col-custom">
        <Footer slides={sliderData} />
      </div>
      <ModalContact />
    </>
  );
};

export default Cars;
