import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Navbar from "./components/Navbar";
import GlobalStyle from "./globalStyles";
import { ModalProvider } from "./context/modalContext";
import Home from "./pages/Home";
import Cars from './pages/Cars';
import { InfoSectionProvider } from "./context/InfoSectionContext";
import { useDarkMode, DarkModeProvider } from "./context/darkModeContext";
import Product from "./pages/Product";

function App() {
  const { darkMode } = useDarkMode();

  return (
    <>
      <DarkModeProvider>
        <ModalProvider>
          <InfoSectionProvider>
            <GlobalStyle darkMode={darkMode} />
            <Router>
              <Navbar  />
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/cars" component = {Cars} />
                <Route path="/product" component = {Product} />
              </Switch>
            </Router>
          </InfoSectionProvider>
        </ModalProvider>
      </DarkModeProvider>
    </>
  );
}

export default App;
