import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
* {
margin: 0;
padding: 0;
box-sizing: border-box;
font-family: 'Montserrat', sans-serif
}

#root {
      overflow: visible;
}

a {
      text-decoration: none;
      outline: none;

      &:hover {
            color: #d6c8c8;
      }
}


body {
      background-color: ${({ darkMode }) => (darkMode ? "#0b0b15" : "#fff")};
  
    }

    .col-6 {
          margin-top:55rem;
          width: 100%;
          @media only screen and (max-width:478px) {
          margin-top: 72rem;
          }
          @media only screen and (max-width:998px) {
          margin-top: 82rem;
          }

          @media only screen and (max-width:375px) {
          margin-top: 84rem;
          }
    }

    .col-3 {
          background-color: #0b0b15;
          width: 100%;
          height: 100%;

        
    }

    .col-3 > section div:first-child div:nth-child(1) {
      background-color: ${({ darkMode }) => (darkMode ? "#0b0b15" : "#fff")};
      height: 60%;
      margin-top: 9rem;
      margin-left: -4rem;
      margin-right: -1rem;

      @media only screen and (max-width:1024px) {
         
                        height: 80%;
                         margin-top: 1rem;
                        margin-left: 0rem;
                        margin-right: -2rem;
            
      }
      @media only screen and (max-width:768px) {
      height: 100%;
    width: 100%;
      }
   
    }

    .col-custom {
          margin-top: -13rem;
    }

    .col-9 {
          width:100%;
          height:100%;
          margin-top: 14rem;

          @media only screen and (max-width: 768px) {
            margin-top: 19rem;
            }
    }
    .col-9__cars {
      margin-top: 8rem;
    }

    .featured-title {
      font-size: clamp(1rem, 4vw, 2rem);
      color: ${({ darkMode }) => (darkMode ? "#fff" : "#000")};
      font-weight: 600;
      text-transorm: uppercase;
      margin: 2rem;
      cursor: pointer;
    }
`;

export default GlobalStyle;
